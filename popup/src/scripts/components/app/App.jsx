import React, {Component} from 'react';
import { Button, ButtonGroup } from 'react-bootstrap';

class App extends Component {
    constructor(props) {
        super(props);
        this.optionClick = this.optionClick.bind(this);
        this.openRandomBookmark = this.openRandomBookmark.bind(this);
        this.openBookmark = this.openBookmark.bind(this);

    }

    openRandomBookmark() {
        var that = this;
        chrome.storage.sync.get('availableBookmarksDirs',function(bookmarksDirs){
            console.log("in popup availableBookmarksDirs: ", bookmarksDirs);
            console.log("in popup availableBookmarksDirs.inside: ", bookmarksDirs.availableBookmarksDirs);
            let randomDir = bookmarksDirs.availableBookmarksDirs[Math.floor(Math.random()*bookmarksDirs.availableBookmarksDirs.length)];
            that.openBookmark(randomDir);
        });

    }

    openBookmark(dirId) {
        let bookmarksUrls = [];
        chrome.bookmarks.getChildren(dirId, function(children) {
            children.forEach(function(bookmark) {
                console.log(bookmark.url);
                bookmarksUrls = bookmarksUrls.concat([bookmark.url]);
            });
            let randomUrl = bookmarksUrls[Math.floor(Math.random()*bookmarksUrls.length)];
            console.log("b length:",bookmarksUrls.length);
            console.log("random bookmark:",randomUrl);
            chrome.tabs.create({url: randomUrl});

        });
    }

    optionClick() {
        //chrome.runtime.openOptionsPage();
        chrome.tabs.create({url: "options.html"})
    }

    render() {
        return (
            <div>
                Bookmark Reminder
                <div >
                    <div className="btn-group btn-group-sm">
                        <Button type="button" className="btn btn-success"> On
                        </Button>
                        <Button type="button" className="btn btn-danger"> Off
                        </Button>
                    </div>
                </div>
                <button type="button" className="btn btn-primary btn-sm btn-link" onClick={this.openRandomBookmark}>
                    Open random bookmark
                </button>
                <button type="button" className="btn btn-primary btn-sm btn-link" onClick={this.optionClick}>
                    Settings
                </button>
            </div>
        );
    }
}

export default App;
