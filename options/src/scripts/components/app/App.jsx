import React, {Component} from 'react';
import { Button, ButtonGroup, Checkbox } from 'react-bootstrap';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectionMode: "all",
            bookmarksDirs: [],
            selectedBookmarksDirs: [],
            tempBookmarks: []
        };

        this.setBookmarks = this.setBookmarks.bind(this);
        this.addBookmarksDir = this.addBookmarksDir.bind(this);
        this.printBookmarksDir = this.printBookmarksDir.bind(this);
        this.saveAvailableBookmarksDirs = this.saveAvailableBookmarksDirs.bind(this);
        this.updateAvailableBookmarksDirs = this.updateAvailableBookmarksDirs.bind(this);
        this.diffArray = this.diffArray.bind(this);
        this.handleSetSelectionChange = this.handleSetSelectionChange.bind(this);
    }

    componentDidMount() {
        var self = this;
        chrome.storage.sync.get('selectionMode', function (data) {
            if (chrome.runtime.lastError) {
                console.error(chrome.runtime.lastError);
            } else {
                self.setState({
                    selectionMode: data.selectionMode
                }, function() {
                    chrome.bookmarks.getTree((bookmarks) => {
                        self.setBookmarks(bookmarks);
                        chrome.storage.sync.get('selectedBookmarksDirs', function (data) {
                            if (chrome.runtime.lastError) {
                                console.error(chrome.runtime.lastError);
                            } else {
                                var selectedBookmarksDirs = [];
                                if (data.selectedBookmarksDirs !== undefined) {
                                    selectedBookmarksDirs = data.selectedBookmarksDirs;
                                }
                                self.setState({
                                    selectedBookmarksDirs: selectedBookmarksDirs
                                }, function() {
                                    self.saveAvailableBookmarksDirs();
                                });
                            }
                        });
                    });
                });
            }
        });
    }

    diffArray(arr1, arr2) {
        var newArr = [];

        arr1.map(function (val) {
            arr2.indexOf(val) < 0 ? newArr.push(val) : '';
        });
        return newArr;
    }

    handleSetSelectionChange(e) {
        this.setState({
            selectionMode: e.target.name
        }, function () {
            this.saveAvailableBookmarksDirs();
            var obj = {};
            obj['selectionMode'] = this.state.selectionMode;
            chrome.storage.sync.set(obj);
        });
    }

    updateAvailableBookmarksDirs(e) {
        var selectedBookmarksDirs = this.state.selectedBookmarksDirs;
        var index = this.state.selectedBookmarksDirs.indexOf(e.target.name);
        if (index > -1) {
            selectedBookmarksDirs.splice(index, 1);
        } else {
            selectedBookmarksDirs = selectedBookmarksDirs.concat([e.target.name]);
        }
        this.setState({
            selectedBookmarksDirs: selectedBookmarksDirs
        }, this.saveAvailableBookmarksDirs);
    }

    saveAvailableBookmarksDirs() {
        var availableBookmarksDirs = [];
        if (this.state.selectionMode == "selected") {
            availableBookmarksDirs = this.state.selectedBookmarksDirs.slice();
        } else if (this.state.selectionMode == "exceptSelected") {
            availableBookmarksDirs = this.diffArray(this.state.bookmarksDirs, this.state.selectedBookmarksDirs);
        } else if (this.state.selectionMode == "all") {
            availableBookmarksDirs = this.state.bookmarksDirs.slice();
        }

        var obj = {};
        obj['availableBookmarksDirs'] = availableBookmarksDirs.slice();
        obj['selectedBookmarksDirs'] = this.state.selectedBookmarksDirs.slice();

        chrome.storage.sync.set(obj, function () {
            if (chrome.runtime.lastError) {
                /* error */
                console.log(chrome.runtime.lastError.message);
            }
        });
    }

    printBookmarksDir() {
        return this.state.tempBookmarks.map((bookmark) => <li key={bookmark.id}><input name={bookmark.id}
                                                                                    checked={this.state.selectedBookmarksDirs.indexOf(bookmark.id) > -1}
                                                                                    onChange={this.updateAvailableBookmarksDirs}
                                                                                    type="checkbox"/> {bookmark.title}</li>);
    }

    addBookmarksDir(prevBookmarksDirs, prevTempBookmarksDirs, directoryId, directoryTitle) {
        if (directoryId != null && directoryId != "") {
            this.setState({
                bookmarksDirs: prevBookmarksDirs.concat([directoryId]),
                tempBookmarks: prevTempBookmarksDirs.concat([{
                    id: directoryId,
                    title: directoryTitle
                }])
            })
        }
    }

    setBookmarks(bookmarks) {
        bookmarks.forEach((bookmark) => {
            if (bookmark.children) {
                this.addBookmarksDir(this.state.bookmarksDirs, this.state.tempBookmarks, bookmark.id, bookmark.title);
                this.setBookmarks(bookmark.children);
            }
        });
    }

    render() {
        return (
            <div>
                <div className="page-header text-center">
                    <h1>Bookmark Reminder</h1>
                </div>
                <hr />
                <div className="page-header">
                    <h1>
                        <small>Bookmarks Set</small>
                    </h1>
                    <div>
                        <label className="checkbox-inline">
                            <input name="selected" type="checkbox" checked={this.state.selectionMode == "selected"}
                                   onChange={ this.handleSetSelectionChange}
                            /> Selected
                        </label>
                        <label className="checkbox-inline">
                            <input name="all" type="checkbox" checked={this.state.selectionMode == "all"}
                                   onChange={ this.handleSetSelectionChange}/> All
                        </label>
                        <label className="checkbox-inline">
                            <input name="exceptSelected" type="checkbox"
                                   checked={this.state.selectionMode == "exceptSelected"}
                                   onChange={this.handleSetSelectionChange}
                            /> All except selected
                        </label>
                        {((this.state.selectionMode == "exceptSelected") || (this.state.selectionMode == "selected")) ?
                            (<div> {this.printBookmarksDir()} </div>) : null}
                    </div>
                </div>
                <div className="page-header">
                    <h1>
                        <small>Reminder Timing</small>
                    </h1>
                    <div>
                        <div>
                            Days
                            <Checkbox>Daily</Checkbox>
                            <Checkbox>Selected days</Checkbox>
                        </div>
                        <div>
                            Frequency
                            <Checkbox>Every open window</Checkbox>
                            <Checkbox>Limited times a day</Checkbox>
                        </div>
                        <div>
                            Time
                            <Checkbox>All day</Checkbox>
                            <Checkbox>Specific hours</Checkbox>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default App;
