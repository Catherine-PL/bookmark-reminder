import React from 'react';
import {render} from 'react-dom';
import Bootstrap from 'bootstrap/dist/css/bootstrap.css';

import App from './components/app/App';

render(
    <App/>
    , document.getElementById('app'));
